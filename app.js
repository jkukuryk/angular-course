const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.API_PORT || 5001;
const router = express.Router();
const compression = require('compression');
const path = require('path');
const app = express();
const isProduction = process.env.NODE_ENV === "production";
const helmet = require('helmet');

app.use(express.static(path.join(__dirname, 'src')));
app.use(express.static(path.join(__dirname, 'src/style')));
app.use(express.static(path.join(__dirname, 'src/style/*')));
app.use(express.static(path.join(__dirname, 'src/js')));
app.use(express.static(path.join(__dirname, 'src/template')));
app.use(express.static(path.join(__dirname, 'src/pages')));
app.set("port", port);

app.use(compression()); //Compress all routes
app.use(helmet()); //protect by helmet

//----------------- | CONFIG APP | ------------------ //
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
let crossOriginAllow = 'http://localhost:8080';

app.use(function(req, res, next) {
    var allowedOrigins = [crossOriginAllow];
    var origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers',
        'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    next();
});
//----------------- | APP ROUTES | ------------------ //
var mockBooks = [
  {id: 1, tytul: 'Ogniem i mieczem', autor: 'Henryk Sienkiewicz', cena: 100, kraj: 'pl'},
  {id: 2, tytul: 'Pan Wołodyjowski', autor: 'Henryk Sienkiewicz', cena: 80, kraj: 'pl'},
  {id: 3, tytul: 'Potop', autor: 'Henryk Sienkiewicz', cena:89, kraj: 'pl'},
  {id: 4, tytul: 'Śmierć w Breslau', autor: 'Marek Krajewski', cena: 70, kraj: 'pl'},
  {id: 5, tytul: 'Mistrz i Małgorzata', autor: 'Michaił Bułhakow', cena: 95, kraj: 'ru'},
  {id: 6, tytul: 'Wilk Stepowy', autor: 'Hermann Hesse', cena: 125, kraj: 'de'},
  {id: 7, tytul: 'Gra szklanych paciorków', autor: 'Hermann Hesse', cena: 75, kraj: 'de'},
  {id: 8, tytul: 'Mistrz i Małgorzata', autor: 'Michaił Bułhakow', cena: 95, kraj: 'ru'},
  {id: 9, tytul: 'Świtezianka', autor: 'Adam Mickiewicz', cena: 20, kraj: 'pl'},
  {id: 10, tytul: 'W 80 dni dookoła świata', autor: 'Verne Jules', cena: 60, kraj: 'fr'},
  {id: 11, tytul: 'Wesele', autor: 'Stanisław Wyspiański', cena: 50, kraj: 'pl'},
  {id: 12, tytu: "Z głowy", autor: 'Janusz Głowacki, ', cena: 35, kraj: 'pl'},
  {id: 13, tytu: "Malowany ptak", autor: 'Jerzy Kosiński ', cena: 73, kraj: 'pl'},
  {id: 14, tytu: "Mała apokalipsa", autor: 'Tadeusz Konwicki ', cena: 54, kraj: 'pl'},
  {id: 15, tytu: "Cierpkość wiśni", autor: 'Izabela Sowa ', cena: 59, kraj: 'pl'},
  {id: 16, tytu: "Pureznto", autor: 'Joanna Bator ', cena: 78, kraj: 'pl'},
  {id: 17, tytu: "Portret w sepii", autor: 'Isabelle Allende', cena: 89, kraj: 'ch'},
  {id: 18, tytu: "Sto lat samotności", autor: 'Gabriel Garcia Marquez ', cena: 34, kraj: 'es'},
  {id: 19, tytu: "Hańba", autor: 'John Maxwell Coetzee', cena: 23, kraj: 'es'},
  {id: 20, tytu: "Hrabia Monte Christo", autor: 'Aleksander Dumas', cena: 37, kraj: 'fr'},
  {id: 21, tytu: "Regulamin tłoczni win", autor: 'Jhon Irving', cena: 54, kraj: 'en'},
  {id: 22, tytu: "Lśnienie", autor: 'Stephen King', cena: 34, kraj: 'us'},
  {id: 23, tytu: "Ewelina i czarny ptak", autor: 'Grzegorz Gortat', cena: 23, kraj: 'pl'},
  {id: 24, tytu: "Pan Ibrahim i kwiaty Koranu", autor: 'Eric Emmanuel Schmitt ', cena: 12, kraj: 'fr'},
  {id: 25, tytu: "Podpalacz", autor: 'Wojciech Chmielarz ', cena: 65, kraj: 'pl'},
  {id: 26, tytu: "Okularnik", autor: 'Katarzyna Bonda ', cena: 67, kraj: 'pl'},
  {id: 27, tytu: "Jak zawsze", autor: 'Zygmunt Miłoszewski ', cena: 88, kraj: 'pl'},
  {id: 28, tytu: "Smutek cinkciarza", autor: 'Sylwia Chutnik ', cena: 34, kraj: 'pl'},
  {id: 29, tytu: "Bieguni", autor: 'Olga Tokarczuk', cena: 25, kraj: 'pl'},
  {id: 30, tytu: "Inna dusza", autor: 'Łukasz Orbitowski ', cena: 96, kraj: 'pl'},
  {id: 31, tytu: "Kochanie, zabiłem nasze koty", autor: 'Dorota Masłowska', cena: 55, kraj: 'pl'},
  {id: 32, tytu: "Bikini", autor: 'Janusz Leon Wiśniewski ', cena: 34, kraj: 'pl'},
  {id: 33, tytu: "Wielkie nadzieje", autor: 'Charles Dickens ', cena: 23, kraj: 'en'},

]
app.route('/api/book')
  .get(function (req, res) {
      res.send(mockBooks);
  })
  .post(function (req, res) {
    res.send('Add a book')
  })
  .put(function (req, res) {
    res.send('Update the book')
  });

app.route('/api/book/:id')
.get(function (req, res) {
  res.send('@TODO - brak obsługi');
})

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'src', 'index.html'));
});
//--------------------------------------------------- //

app.listen((port), () => {
    console.log(`Server listening on ${port}`);
});
