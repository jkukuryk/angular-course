module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					compass: true,
					style: 'expanded'
				},
				files: {
					'style/app.css': 'sass/app.scss'
				}
			}
		},
		imagemin: {
		    pngs: {
		        options: {
		            progressive: true
		        },
		        files: [{
		            expand: true,
		            cwd: 'sass/assets',
		            src: ['*.png'],
		            dest: 'style/assets'
		        }]
		    }
		},
		watch: {
			css: {
        files: ['sass/*.scss'],
				tasks: ['sass']
			}
		},
  });

	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-run');
	grunt.registerTask('default', ['sass', 'imagemin']);
};
