module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'node_modules/angular/angular.js',
      'node_modules/angular-route/angular-route.js',
      'node_modules/angular-animate/angular-animate.js',
      'node_modules/angular-ui-bootstrap/animate/angular-animate.js',
      'node_modules/angular-translate/dist/angular-translate.min.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'js/*.js',
      'js/**/*.js',
      'js/**/*.test.js',
    ],
    exclude: [
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    plugins: ['karma-phantomjs-launcher','karma-jasmine'],
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  })
}
