app.config(function($translateProvider) {
  $translateProvider
      .translations('en', translateEn)
      .translations('pl', translatePl)
      .preferredLanguage('en');
});

var translatePl = {
  addBook: {
    title: 'Tytuł',
  }
}

var translateEn = {
  addBook: {
    title: 'Title',
  }
}
