app.config(function($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$routeProvider.when("/", {
			templateUrl: "pages/simpleDirectives.html"
		})
		.when("/simpleDirective", {
			templateUrl: "pages/simpleDirectives.html"
		})
		.when("/customDirective", {
			templateUrl: "pages/customDirectives.html"
		})
		.when("/myFilter", {
			templateUrl: "pages/myFilter.html",
		})
		.when("/addBook", {
			templateUrl: "pages/addBook.html",
			controller: "addBookController",
		})
		.when("/ajax", {
			templateUrl: "pages/ajax.html",
			controller: "ajaxController",
		})
		.otherwise({ redirectTo: '/' });
});
