app.directive('validPrice', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, mCtrl) {
			//value = 3
			function setValidity(value) {
				if(!value) {
					mCtrl.$setValidity('validPrice', false);
					return value;
				}
				var valueNumber = parseInt(value, 10);

				if(!isNaN(valueNumber) && valueNumber > 50 &&  valueNumber < 9999) {
					mCtrl.$setValidity('validPrice', true);
				} else {
					mCtrl.$setValidity('validPrice', false);
				}
				return value;
			}
			mCtrl.$parsers.push(setValidity);
		}
	};
});
