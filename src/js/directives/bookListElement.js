app.directive("bookListElement", function() {
  return {
    template: "<li>{{ksiazka.autor | uppercase }} - {{ksiazka.tytul}} cena: {{ksiazka.cena | cenaPl}}</li>",
    scope: {
      ksiazka: '=',
    },
    replace: true,
  };
})
