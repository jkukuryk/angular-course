app.directive("linkTo", function() {
	return {
		templateUrl: 'template/link.htm',
		scope: {
			link: '='
		},
		replace: true,
		controller: function($rootScope, $scope, $location, $routeParams) {
			var baseClass = 'green ';
			function getClass(isActive) {
				var newClass = isActive ? 'active' : '';
				return baseClass + newClass;
			};
			console.log('$location',$location, $location.$$path);
			$scope.buttonClass = $location.path() === $scope.link.url ? "active" : "";

			$rootScope.$on('$routeChangeSuccess', function(e, current) {
				$scope.buttonClass = getClass($location.path() === $scope.link.url);
			});

	 		$scope.gotoPath = function(path) {
				$location.path(path);
			}
		},
	};
})
