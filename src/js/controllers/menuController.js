app.controller("menuController", function($scope, $translate) {

  $scope.$on('$routeChangeSuccess', function(e,current) {
    console.log('$routeChangeSuccess', current);
  });

  $scope.changeLanguage = function(langKey) {
    $translate.use(langKey);
  };

  $scope.pages = [
    {url: '/', label: 'Main page'},
    {url: '/simpleDirective', label: 'simpleDirective'},
    {url: '/customDirective', label: 'customDirective'},
    {url: '/myFilter', label: 'myFilter'},
    {url: '/addBook', label: 'dodaj ksiażkę'},
    {url: '/ajax', label: 'ajax'},
  ];
});
