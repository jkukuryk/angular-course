describe('addBookController', function() {
  var $scope = null;
  var ctrl = null;
  var bookFactory;
  beforeEach(module('app'));
  beforeEach(inject(function($rootScope, $controller, _bookFactory_) {
    $scope = $rootScope.$new();
    bookFactory = _bookFactory_;

    ctrl = $controller('addBookController', {
      $scope: $scope,
      bookFactory: bookFactory,
    });
  }));

  it("bookFactory should have functions as params", function() {
    expect(typeof bookFactory._get).toBe('function');
    expect(typeof bookFactory._set).toBe('function');
    expect(typeof bookFactory.wyczyscListe).toBe('function');
    expect(typeof bookFactory.remove).toBe('function');
  });

  it('Scope zapisaneKsiazki should be from factory', function () {
    expect($scope.zapisaneKsiazki.length).toBe(0);
  });
});
