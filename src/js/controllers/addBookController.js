app.controller("addBookController", function($scope, $location, bookFactory) {
		$scope.tytul = '';
		$scope.autor = '';
		$scope.cena = '';
		$scope.showErrors = false;
		$scope.zapisaneKsiazki = bookFactory._get();
		$scope.zresetujPola = function() {
			$scope.tytul = '';
			$scope.autor = '';
			$scope.cena = '';
		}
		$scope.isModalOpen = false;
		$scope.zapiszKsiazke = function() {

			if($scope.bookForm.$valid) {
				$scope.zapisaneKsiazki = bookFactory._set({
					tytul: $scope.tytul,
					autor: $scope.autor,
					cena: $scope.cena,
				});
				$scope.zresetujPola();
			}
		}
		$scope.usunCalaListe = function() {
			$scope.zapisaneKsiazki = bookFactory.wyczyscListe();
			zamknijModal();
		}

		function zamknijModal() {
			$scope.isModalOpen = false;
		}
		$scope.zamknijModal = function() {
			zamknijModal();
		}
		$scope.wyczyscListe = function() {
			$scope.isModalOpen = true;
		}
		$scope.nieTanie = function(ksiazka) {
			return ksiazka.cena >= 50;
		}

		$scope.checkValid = function() {
			console.log('$scope.tytul',$scope.tytul);
		}
	})
	.filter("cenaPl", function($filter) {
		return function(cena) {
			var filteredPrice = $filter('currency')(cena, '')
				.replace(',', ' ')
				.replace('.', ',');
			return filteredPrice + 'zł';
		}
	})
