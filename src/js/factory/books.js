app.factory('bookFactory', function() {
	var bookList = [];
	var factoryReturn = {};
	factoryReturn._get = function() {
		return bookList;
	}
	factoryReturn._set = function(ksiazka) {
		ksiazka.id = bookList.length + 1;
		bookList.push(ksiazka);
		return bookList;
	}
	factoryReturn.wyczyscListe = function() {
		bookList = [];
		return bookList;
	}
	factoryReturn.remove = function(id) {
		var newBooks = [];
		forEach(bookList, function(ksiazka, idnex) {
			if(ksiazka.id !== id) {
				newBooks.push(ksiazka);
			}
		});
		bookList = newBooks;
		return bookList;
	}
	return factoryReturn;
});
